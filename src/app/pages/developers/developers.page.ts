import { map } from "rxjs/operators";
import { DatabaseService } from "./../../services/database.service";
import { Observable } from "rxjs";
import { Component, OnInit } from "@angular/core";
import { Dev } from "src/app/services/database.service";

@Component({
  selector: "app-developpers",
  templateUrl: "./developers.page.html",
  styleUrls: ["./developers.page.scss"],
})
export class DevelopersPage implements OnInit {
  developers: Dev[] = [];

  products: Observable<any[]>;

  developer: {};
  product = {};

  selectedView = "devs";

  constructor(private db: DatabaseService) {}

  ngOnInit() {
    this.db.getDatabaseState().subscribe((rdy) => {
      if (rdy) {
        this.db.getDevs().subscribe((devs) => {
          this.developers = devs;
        });
        this.products = this.db.getProducts();
      }
    });
  }

  addDeveloper() {
    let skills = this.developer["skills"].split(",");
    skills = skills.map((skill) => skill.trim());

    this.db
      .addDeveloper(this.developer["name"], skills, this.developer["img"])
      .then((_) => {
        this.developer = {};
      });
  }

  addProduct() {
    this.db
      .addProduct(this.product["name"], this.product["creator"])
      .then((_) => {
        this.developer = {};
      });
  }
}
