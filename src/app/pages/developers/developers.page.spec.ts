import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DeveloppersPage } from './developpers.page';

describe('DeveloppersPage', () => {
  let component: DeveloppersPage;
  let fixture: ComponentFixture<DeveloppersPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeveloppersPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DeveloppersPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
